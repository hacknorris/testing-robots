async function startProgram() {
	setStabilization(true);
	if (true) {
		await spinner();
	} else {
		await multidimension();
	}
	exitProgram();
}

async function spinner() {
	await spin(999, 15);
	await spin(9999, 15);
	await spin(99999, 15);
	await spin(999999, 15);
}

async function multidimension() {
	await rawMotor(100, -100, 30);
	await rawMotor(150, -150, 30);
	await rawMotor(200, -200, 30);
	await rawMotor(255, -255, 90);
}

// just a script testing you how long you can hanle spinning bot in your hand
// warning - bot need to be FULLY charged and you'll run it on your hand so i wont handle your damage lool
