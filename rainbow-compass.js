async function startProgram() {
	await calibrateCompass();
	resetAim();
	setCompassDirection(0);
	playMatrixAnimation(0, false);
}

registerMatrixAnimation({
	frames: [[[1, 1, 1, 2, 2, 1, 1, 1], [1, 1, 1, 3, 3, 1, 1, 1], [1, 1, 1, 4, 4, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 8, 8, 1, 1, 1], [1, 1, 1, 10, 10, 1, 1, 1], [1, 1, 1, 11, 11, 1, 1, 1]]],
	palette: [{ r: 255, g: 255, b: 255 }, { r: 0, g: 0, b: 0 }, { r: 255, g: 0, b: 0 }, { r: 255, g: 64, b: 0 }, { r: 255, g: 128, b: 0 }, { r: 255, g: 191, b: 0 }, { r: 255, g: 255, b: 0 }, { r: 185, g: 246, b: 30 }, { r: 0, g: 255, b: 0 }, { r: 185, g: 255, b: 255 }, { r: 0, g: 255, b: 255 }, { r: 0, g: 0, b: 255 }, { r: 145, g: 0, b: 211 }, { r: 157, g: 48, b: 118 }, { r: 255, g: 0, b: 255 }, { r: 204, g: 27, b: 126 }],
	fps: 10,
	transition: MatrixAnimationTransition.None
});
