
const type = "binary" // type "normal" to get time as number lines or "binary" to get binary clock

const backgroundColor = ({ r: 255, g: 255, b: 0 }); // here change color of clock borders. only binary clock
const inactiveColor = ({ r: 0, g: 255, b: 255 });  // here change inactive dots of time. binary clock only
const activeColor = ({ r: 255, g: 0, b: 255 }); // here change text OR active time dots color

registerEvent(EventType.onCharging, onCharging);



async function startProgram() {
/*	if (type == "normal"){
		normalClock();
	}
	if (type == "binary"){
		binaryClock();
	}*/
}

async function toBinary() {
	const hour = (Math.floor((getCurrentTime() % 86400) / 3600) + 2);
	const minute = Math.floor((getCurrentTime() % 3600) / 60);
	const second = (Math.floor((getCurrentTime() % 3600) % 60));
	switch (hour) {
		case 1:
			var binaryHour = 1
			break;
		case 0:
			var binaryHour = 0
			break;
		default:
			var binaryHour = toBinary(Math.floor(hour / 2)) + (hour % 2);
			break;
	}
	switch (minute) {
		case 1:
			var binaryMinute = 1
			break;
		case 0:
			var binaryMinute = 0
			break;
		default:
			var binaryMinute = toBinary(Math.floor(minute / 2)) + (minute % 2);
			break;
	}
	switch (second) {
		case 1:
			var binarySecond = 1
			break;
		case 0:
			var binarySecond = 0
			break;
		default:
			var binarySecond = toBinary(Math.floor(second / 2)) + (second % 2);
			break;
	}
}
async function binaryClock(){
	const hour = (Math.floor((getCurrentTime() % 86400) / 3600) + 2);
	const minute = Math.floor((getCurrentTime() % 3600) / 60);
	const second = (Math.floor((getCurrentTime() % 3600) % 60));
	drawMatrixLine(backgroundColor, { x: 0, y: 0 }, { x: 7, y: 0 }); //
	drawMatrixLine(backgroundColor, { x: 0, y: 1 }, { x: 7, y: 1 }); //
	drawMatrixLine(backgroundColor, { x: 0, y: 2 }, { x: 7, y: 2 });
	drawMatrixLine(backgroundColor, { x: 0, y: 6 }, { x: 7, y: 6 }); 
	drawMatrixLine(backgroundColor, { x: 0, y: 7 }, { x: 7, y: 7 });
	drawMatrixLine(backgroundColor, { x: 2, y: 0 }, { x: 2, y: 7 });
	drawMatrixLine(backgroundColor, { x: 5, y: 0 }, { x: 5, y: 7 });
	drawMatrixFill(inactiveColor, {x: 0 ,y:3 }, {x:1 ,y:5});
	drawMatrixFill(inactiveColor, {x: 3 ,y:3 }, {x:4 ,y:5});
	drawMatrixFill(inactiveColor, {x: 6, y:3 }, {x:7, y:5});
	
	let time = (second + minute + hour).split('');
	var binaryTime = '';
	var matrix = [{x:6,y:3},{x:7,y:3},{x:6,y:4},{x:7,y:4},{x:6,y:5},{x:7,y:5},{x:3,y:3},{x:4,y:3},{x:3,y:4},{x:4,y:4},{x:3,y:5},{x:4,y:5},{x:0,y:3},{x:1,y:3},{x:0,y:4},{x:1,y:4},{x:0,y:5},{x:1,y:5}];

	function zeroFill(number, width) {
        width -= number.toString().length;
        if (width > 0) {
            return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
        }
        return number + "";
    }
	
	for (let el of time) {
        binaryTime += this.zeroFill(Number.parseInt(el).toString(2), 4);
    }
	
	for (let index in binaryTime.split('')) {
    	if (binaryTime[index] == 0) {
        	drawMatrixPixel(activeColor,matrix[index]);
        } else {
            drawMatrixPixel(inactiveColor,matrix[index]);
        }
    }
}

async function normalClock() {
	const hour = (Math.floor((getCurrentTime() % 86400) / 3600) + 2);
	const minute = Math.floor((getCurrentTime() % 3600) / 60);
	const second = (Math.floor((getCurrentTime() % 3600) % 60));
	await scrollMatrixText(buildString(hour,":", minute,".", second), activeColor,15, true);
}

async function onCharging() {

	if (type == "normal"){
		normalClock();
	}
	if (type == "binary"){
		binaryClock();
	}
}